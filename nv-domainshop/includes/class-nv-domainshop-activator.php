<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bobby.xyz
 * @since      1.0.0
 *
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 * @author     Bobby Thompson <bobby@tech.haus>
 */
class Nv_Domainshop_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
