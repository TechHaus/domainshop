<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bobby.xyz
 * @since      1.0.0
 *
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 * @author     Bobby Thompson <bobby@tech.haus>
 */
class Nv_Domainshop_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'nv-domainshop',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
