<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bobby.xyz
 * @since      1.0.0
 *
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nv_Domainshop
 * @subpackage Nv_Domainshop/includes
 * @author     Bobby Thompson <bobby@tech.haus>
 */
class Nv_Domainshop_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
