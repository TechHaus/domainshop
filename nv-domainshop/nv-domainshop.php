<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bobby.xyz
 * @since             1.0.0
 * @package           Nv_Domainshop
 *
 * @wordpress-plugin
 * Plugin Name:       DomainShop
 * Plugin URI:        https://tech.haus
 * Description:       Adds Custom Product Type "Domain" to WooCommerce.
 * Version:           0.1.0
 * Author:            Bobby Thompson
 * Author URI:        https://bobby.xyz
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nv-domainshop
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'NV_DOMAINSHOP_VERSION', '0.1.0' );

if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . '/wp-admin/includes/plugin.php');
}

/**
* Check for the existence of WooCommerce and any other requirements
*/
function nv_check_requirements() {
    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        return true;
    } else {
        add_action( 'admin_notices', 'nv_missing_wc_notice' );
        return false;
    }
}

/**
* Display a message advising WooCommerce is required
*/
function nv_missing_wc_notice() { 
    $class = 'notice notice-error';
    $message = __( 'DomainShop requires WooCommerce to be installed and active.', 'nv-domainshop' );
 
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nv-domainshop-activator.php
 */
function activate_nv_domainshop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nv-domainshop-activator.php';
	Nv_Domainshop_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nv-domainshop-deactivator.php
 */
function deactivate_nv_domainshop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nv-domainshop-deactivator.php';
	Nv_Domainshop_Deactivator::deactivate();
}

add_action( 'plugins_loaded', 'nv_check_requirements' );

register_activation_hook( __FILE__, 'activate_nv_domainshop' );
register_deactivation_hook( __FILE__, 'deactivate_nv_domainshop' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nv-domainshop.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nv_domainshop() {
    if (nv_check_requirements()) {
        $plugin = new Nv_Domainshop();
//         $plugin->run();
    }
}

run_nv_domainshop();
