# DomainShop Plugin for WooCommerce

DomainShop is a plugin for WooCommerce that creates a custom product type called Domain. 

## Future Plans

Coming soon.

## Thanks

A big thank you to the folks who made the [WordPress Plugin Boilerplate](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate) and the [WordPress Plugin Boilerplate Generator](https://wppb.me/).

Two articles I found super helpful to get started: [The WordPress Plugin Boilerplate 101: How to Get Started](https://www.codeinwp.com/blog/the-wordpress-plugin-boilerplate-101/) and to [How to Extend WooCommerce with the WordPress Plugin Boilerplate](https://medium.com/@paulmiller3000/how-to-extend-woocommerce-with-the-wordpress-plugin-boilerplate-adac178b5a9b).

